from django.apps import AppConfig


class WarDecConfig(AppConfig):
    name = "wardec"
    label = "wardec"
    verbose_name = "War Declaration Notifications"
