from django.utils.translation import ugettext_lazy as _

from allianceauth import hooks
from allianceauth.services.hooks import MenuItemHook, UrlHook

from . import urls


class WarDecMenuItem(MenuItemHook):
    """ This class ensures only authorized users will see the menu entry """

    def __init__(self):
        # setup menu entry for sidebar
        MenuItemHook.__init__(
            self,
            _("wardec"),
            "fas fa-cube fa-fw",
            "wardec:index",
            navactive=["wardec:"],
        )

    def render(self, request):
        if request.user.has_perm("wardec.basic_access"):
            return MenuItemHook.render(self, request)
        return ""


@hooks.register("menu_item_hook")
def register_menu():
    return WarDecMenuItem()


@hooks.register("url_hook")
def register_urls():
    return UrlHook(urls, "wardec", r"^wardec/")
