from django.urls import path

from . import views

app_name = "wardec"

urlpatterns = [
    path("", views.index, name="index"),
]
